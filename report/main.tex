\documentclass[runningheads]{llncs}
\usepackage{llncsdoc}

\usepackage{hyperref}

\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc}
\usepackage{siunitx} % SI units
\usepackage{booktabs} % Nice tables
\usepackage{tikz}
\usetikzlibrary{math}
\usepackage{ifthen}
\usepackage{standalone}

\usepackage{amsmath}

\usepackage{pgfplots}
% colors
\definecolor{color1}{HTML}{1f77b4}
\definecolor{color2}{HTML}{ff7f0e}
\definecolor{color3}{HTML}{2ca02c}

\setcounter{secnumdepth}{3}% Number up to \subsubsection

\makeatletter
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                       {-18\p@ \@plus -4\p@ \@minus -4\p@}%
                       {0.5em \@plus 0.22em \@minus 0.1em}%
                       {\normalfont\normalsize\bfseries\boldmath}}
\makeatother

\makeatletter
\newcommand*{\centerfloat}{%
  \parindent \z@
  \leftskip \z@ \@plus 1fil \@minus \textwidth
  \rightskip\leftskip
  \parfillskip \z@skip}
\makeatother

\usepackage{graphicx}
\graphicspath{{./pictures/}}

\begin{document}
    \title{Ludo Playing AI using an Artificial Neural Network and Optimization using a Genetic Algorithm.}
    \titlerunning{Ludo Playing AI using an ANN and Optimization using a GA.}
    \author{Emil Lykke Diget}
    \institute{University of Southern Denmark, Campusvej 55, 5230 Odense M, Denmark \\ \href{mailto:emdig16@student.sdu.dk}{\url{emdig16@student.sdu.dk}}}
    \maketitle

    \nocite{*}

    \begin{abstract}
        An artificial neural network (ANN) for playing ludo was developed.
        It used a state representation of the game board as input and returned which token to move.
        The performance of the ANN was increased evolutionary by a genetic algorithm (GA).

        The developed ANN+GA player was tested against four different players; original ANN, random, semismart and a Q-learning player developed by Tinggaard in \cite{tinge2020}.
        It was concluded that the ANN+GA player was better than the four other players.
    \end{abstract}

    \section{Introduction}
        In this paper a Ludo playing \textit{Artificial Intelligence} (AI) is introduced.
        The AI is composed of an \textit{Artificial Neural Network} (ANN) that decides which token to move depending of the current state of the game.
        Ludo is a board game for four players where each player has four coloured tokens.
        This paper uses a C\texttt{++} of the game written by Rasmus Donbæk \cite{ludogame}.
        The Ludo player developed in this paper will be pitted against Christian Tinggaards Ludo player developed using Q-learning \cite{tinge2020}.
        The source code for the project can be found on GitLab\footnote{\url{https://gitlab.com/emilld/Ludo-help-for-tools-of-AI}}.

        \subsection{Rules of Ludo}
            In this section the rules of the Ludo implementation will be described.
            The game consists of four players racing clockwise around the board as seen on \autoref{fig:ludoboard}.
            Each player has four tokens in a unique colour.
            In the beginning of the game all the tokens are in the house of each player, that is the square with four squares inside in the corners of the board.
            To get a token out of the house a player has to roll a 6.
            A player must always move a token according to the die roll.
            If this is not possible the player is skipped.
            If a player moves a token to the same spot as another player's token the other player's token will be send home.
            If a token hits a star it is teleported to the next star.
            If a token from an another player is present at the second star it is send home.
            A globe grants immunity and a token on it will send other tokens home if they go here.
            If a player stacks two or more of his own tokens on the same spot then it will behave as a globe.
            The winning stretch is the last part of the board.
            If a token hits the star in front of it's corresponding colour, then it will go straight to goal.
            To enter the goal the die roll must match exactly with the number of moves left, otherwise the token will move into the goal and then backwards again.
            The game comes to a conclusion when a player have all his tokens in the goal.
            
            \begin{figure}[!tbh]
                \centering
                \includegraphics[width=0.4 \textwidth]{pictures/ludo_board.eps}
                \caption{An illustration of a Ludo board. Illustration by the Wikimedia Commons user Cmglee distributed under a CC BY-SA 4.0 licence. The illustration has been altered to hide the movements of the tokens and to show the stars and the globes.}
                \label{fig:ludoboard}
            \end{figure}


    % \section{Bacground}


    \section{Methods}
        In this section the methods are described.
        To begin with a state representation of the game is made.
        Afterwards a player is developed using an ANN and further improved using a \textit{Genetic Algorithm} (GA).
        In the end the Q-learning player by Tinggaard is briefly described \cite{tinge2020}.

        \subsection{State Representation}
            The Ludo implementation stores the positions of the tokens as a vector of absolute positions, as explained in \autoref{fig:abspos}.
            This representation is transformed into a vector of relative positions as seen on \autoref{fig:relpos} where the green player is the current player.
            In the Ludo implementation a player is presented with his relative position vector which is 16 values long; a spot for each token describing its current position.
            As seen on the figures a token has the value $-1$ when at home and a value of 99 when it's in the goal.

            \begin{figure}[!tbh]
                \centerfloat
                \begin{minipage}[t]{0.6\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{pictures/absolute_position.png}
                    \caption{Explanation of the absolute position of tokens by Rasmus Donbæk \cite{ludogame}.}
                    \label{fig:abspos}
                \end{minipage}\qquad
                \begin{minipage}[t]{0.6\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{pictures/relative_position.png}
                    \caption{Explanation of the relative position of the tokens by Rasmus Donbæk \cite{ludogame}.}
                    \label{fig:relpos}
                \end{minipage}
            \end{figure}
            
            An abstract representation of the current state of each of a players tokens is made so it is more accessible to an AI.
            The state is represented as a \SI{15}{bit} binary string made up by the states presented in \autoref{tab:state_rep}. 
            It consists of \SI{8}{bits} describing the current state of the token and \SI{7}{bits} describing the future state of the token if it will be moved.
            By having both present and possible future state of the tokens the AI will be able to predict the outcome of a move and not only acting on the present situation.
            Each player has four tokens with each \SI{15}{bit} which results in a string of \SI{60}{bit} to describe the state of all tokens of a player.

            \begin{table}[!tbh]
                \centering
                \caption{A list of the bits representing the state of a token.}
                \begin{tabular}{*2l}
                    \toprule
                    Bit & Name \\ 
                    \midrule
                    0 & Token is free \\
                    1 & Token is dead \\
                    2 & Token is standing on a star \\ 
                    3 & Token is standing on a globe \\ 
                    4 & Token is on the goal stretch \\
                    5 & Token is finished \\ 
                    6 & Token is on enemy start \\
                    7 & Token is forming a blockage \\
                    \midrule
                    8 & Token can form a blockage \\
                    9 & Token can kill an enemy token \\
                    10 & Token can reach goal \\
                    11 & Token can leave home \\
                    12 & Token can move to a star \\
                    13 & Token can go to a globe \\
                    14 & Token can die \\
                    \bottomrule
                \end{tabular}
                \label{tab:state_rep}
            \end{table}


        \subsection{Artificial Neural Network} \label{subsec:ann}
            In this section the used ANN will be described and choices will be explained.
            The ANN used in this paper is of the type \textit{Fully Connected Multilayer Perceptron} but will just be referenced later in the text as \textit{ANN}.
            The ANN is implemented using \textit{Fast Artificial Neural Network} (FANN) \cite{fann_doc}.
            As the ANN controls the tokens of a single player it should have an input layer with a size of 60 to use the state described in the last section.
            The ANN should choose wich token it would be most favorable to move with, so the output layer should have a size of 4.
            In this paper one hidden layer is used.
            There is no really strict rule on how many neurons there should be in the hidden layer.
            % A common rule of thumb is to use the average of the input and the output layer which in this case will be $(60 + 4) / 2 = 32$.
            By trial and error the number of neurons in the hidden layer has been found as 6.
            The ANN also has a bias neuron on the input and the hidden layer, thus the total number of neurons in the network is: $ 61 + 7 + 4 = 72 $.
            The ANN is illustrated on \autoref{fig:ann} with data propagating from left to right through the input-, the hidden-, and the output layer. 

            \begin{figure}[!tbh]
                \centering
                \input{pictures/ann_topology.tex}
                \caption{An illustration of the ANN.}
                \label{fig:ann}
            \end{figure}
            
            The input and the output has been scaled to be between $-1$ and $1$ as the training time is reduced \cite{2005nn}.
            $-1$ corresponds to false and $1$ corresponds to true.
            Due to this scaling the ANN is using the $\tanh$-activation function.
            The weights of the ANN is initialized randomly in the range $[-0.77 ; 0.77]$ as this gives the best mean performance as shown in \cite{1997init}.
            To train the network a training set has been made by playing 10 games manually and recording the state of the players four tokens together with wich token was selected to move.
            This corresponds to 844 moves.
            Different learning algorithms are available in FANN.
            Two types of standard backpropagation algoirithm; incremental and batch.
            The incremental one update the weights after each training pattern, thus several times an epoch, while the batch updates the weights after each epoch.
            There is also an implementation of RPROP algorithm which has superior performance compared to ordinary gradient descent by backpropagation \cite{Riedmiller93adirect}.
            The three algorithms are compared on \autoref{fig:learning_algs} by training for \SI{250}{epochs} on the training set.
            The learning rate for the incremental algorithm and the batch algoirithm is set to $0.7$ by trial and error.
            The parameters of the RPROP algorithm is kept at default as the produced result works nicely.
            It can be seen that the incremental algorithm produce a very noisy result while also not producing a very low MSE.
            The batch algorithm produces a smoother result and concludes in a lower MSE than the incremental algorithm.
            The RPROP algorithm both produce a much smoother MSE curve compared to the two others and it converges faster and lower.
            The RPROP is selected as the learning algorithm.

            \begin{figure}[!tbh]
                \centering
                \begin{tikzpicture}
                    \begin{axis}[
                            xlabel={Epochs},
                            ylabel={MSE},
                            grid=major,
                            xmin = 0,
                            xmax = 250,
                            width=0.8\textwidth,
                            height=5cm,
                            legend pos= north east,
                            x tick label style={/pgf/number format/.cd,%
                                scaled x ticks = false,
                                set decimal separator={,},
                                fixed
                            }
                        ]
                    
                        % Graph column 2 versus column 0
                        \addplot+[very thick, solid, mark=none, color1] table[x index=0,y index=1,col sep=comma] {data/mse_incremental_avg.txt};
                        \addlegendentry{Incremental}% y index+1 since humans count from 1
                        
                        % Graph column 1 versus column 0    
                        \addplot+[very thick, dashed, mark=none, color2] table[x index=0,y index=1,col sep=comma] {data/mse_batch_avg.txt};
                        \addlegendentry{Batch}
                        
                        % Graph column 1 versus column 0    
                        \addplot+[very thick, dashdotted, mark=none, color3] table[x index=0,y index=1,col sep=comma] {data/mse_rprop_avg.txt};
                        \addlegendentry{RPROP}

                    \end{axis}
                \end{tikzpicture}
                \caption{Comparison of three available learning algorithms.}
                \label{fig:learning_algs}
            \end{figure}


        \subsection{Genertic Algorithm}
            To improve upon the performance of the ANN player a \textit{Genetic Algorithm} (GA) can be utilized to evolutionarily improve the performance.
            The GA used in this paper is similar to the simple genetic algorithm presented \cite[pp. 8--10]{1999intro2ga}.
            The population size was determined by trial and error to be 30 individuals.
            This is considered to be enough individuals to ensure genetic diversity in the population.

            \subsubsection{Genom Representation}
                The weights of the ANN trained in \autoref{subsec:ann} should be optimized.
                To represent the weights of the ANN as a genom, the genom is defined as a vector of floating point values with a length of:

                \begin{align}
                    \begin{aligned}
                        &(60 \text{ input neurons} + 1 \text{ bias neuron} ) \cdot 6 \text{ hidden neurons} \\
                        + &(6 \text{ hidden neurons} + 1 \text{ bias neuron} ) \cdot 4 \text{ output neurons} \\
                        = & 394 \text{ connections} \enspace .
                    \end{aligned}
                \end{align}

            \subsubsection{Initialization}
                The population is initialized as 30 copies of the ANN weights trained in \autoref{subsec:ann}.
                Normal noise with $\mu = 0$, $\sigma = 1$ is added to every value of the genoms.
                The $\sigma$ was choosen randomly.

            \subsubsection{Evaluation}
                The genoms are in turn evaluated according to their win rate against three semismart players.
                To evaluate a genom 250 games are conducted.
                On the one hand the number of games should be high enough for the influence of the dice to be low and on the other be low enough so that computational time is reasonable.
                Thus the fittness of a genom is defined as: 

                \begin{equation}
                    \text{fitness} = \frac{\text{wins}}{\text{games}} \enspace ,
                \end{equation}
                
                where wins are number of \textit{wins} of the genom and \textit{games} are number of games that was played, in this case 250.

            \subsubsection{Elitism}\label{subsubsec:elitism}
                To preserve some of the genoms of the old population \textit{elitism} is used.
                A certain percentage of the best of the old population is directly carried over to the new population.
                To choose the percentage of elitism some preliminary tests have been performed with results showing on \autoref{fig:elitismMutation} with each of the graphs representing three runs of 1000 generations of the GA.
                Two elitism values and two mutation rates are tested producing four experiments with the following combinations: [10\% elitism, 10\% mutation], [10\% elitism, 50\% mutation], [25\% elitism, 10\% mutation] and [25\% elitism, 50\% mutation].
                Looking at the graph the [10\% elitism, 10\% mutation] or the [10\% elitism, 50\% mutation] combination shows the most promise.
                The [10\% elitism, 10\% mutation] was chosen.

                \begin{figure}[!tbh]
                    \centering
                    \includegraphics[width=0.8\textwidth]{generations_comparison.eps}
                    \caption{Test of different elitism and mutation rates.}
                    \label{fig:elitismMutation}
                \end{figure}

            \subsubsection{Selection}
                To select which two genoms should mate the tournament selection scheme is used.
                Among the $50 \%$ fittest of the population four contestants are choosen at random to compete in a tournament.
                250 games are played and the two most winning players are choosen for mating.
                This number of games is choosen following the same reasoning presented in \autoref{subsubsec:elitism}.
            
            \subsubsection{Crossover}
                The mating of two genoms is done in the crossover step.
                A uniform crossover with a rate of $50\%$ is used to not favor one or the other parent.    
            
            \subsubsection{Mutation}
                Mutation is performed on the new offspring to encourage exploration of the problem space.
                Each value of the genom has a chance of mutation, denoted as \textit{mutation rate}.
                To mutate a value a random number from $\mathcal{N}(0, 2)$ is added.
                The rate of mutation is, along with elitism rate, found by the preleminary test shown in \autoref{fig:elitismMutation}.
                As written earlier a mutation rate of 10\% was chosen.

            \subsubsection{Training}
                The chosen elitism rate of $10\%$ and mutation rate of $10\%$ was trained for 10,000 generations to be sure, the population couldn't get any bettter.
                The result of this training can be seen on \autoref{fig:ga_training}.
                The win rates of the population stabilises after around 5000 generations at a fitness of around $0.54$.
                This player is the one used as ANN+GA in \autoref{sec:results}.

                \begin{figure}[!tbh]
                    \centering
                    \begin{tikzpicture}
                        \begin{axis}[
                                xlabel={Generations},
                                ylabel={Fitness},
                                grid=major,
                                xmin = 0,
                                xmax = 10000,
                                width=0.8\textwidth,
                                height=5cm,
                                legend pos= south east,
                                x tick label style={/pgf/number format/.cd,%
                                    scaled x ticks = false,
                                    set decimal separator={,},
                                    fixed
                                }
                            ]
                        
                            % Graph column 2 versus column 0
                            \addplot+[very thick, solid, mark=none, color1] table[x index=0,y index=1,col sep=comma] {data/best_progress.txt};
                            \addlegendentry{Max}
                            
                            \addplot+[very thick, solid, mark=none, color2] table[x index=0,y index=2,col sep=comma] {data/best_progress.txt};
                            \addlegendentry{Mean}

                            \addplot+[very thick, solid, mark=none, color3] table[x index=0,y index=3,col sep=comma] {data/best_progress.txt};
                            \addlegendentry{Min}
                        \end{axis}
                    \end{tikzpicture}
                    \caption{Evolutionarily training of the ANN player.}
                    \label{fig:ga_training}
                \end{figure}
    

            
            % The genetic algorithm used in this paper has the following steps:

            % \begin{enumerate}
            %     \item Initialize population.
            %     \item Evaluate population so that each genom gets a fitness score.
            %     \item Elitism; carry some of the old population over to the new one.
            %     \item Select two parents.
            %     \item Reproduce the two parents by combining the genoms using crossover.
            %     \item Mutate the offspring and append it to the new population.
            %     \item Repeat step 4 -- 6 until the new population is of the same size of the previous population.
            %     \item A generation is done. Start over from step 2 to generate more generations. Stop the process when a number of generations is reached.
            % \end{enumerate}

            % Each step will be described in detail underneath.

            % \subsubsection{Initialization}

            % \subsubsection{Evaluation}

            % \subsubsection{Elitism}

            % \subsubsection{Selection}

            % \subsubsection{Reproduction}

            % \subsubsection{Mutation}
        
        \subsection{Development of a Q-Learning Player \cite{tinge2020}}
            In this section the Q-learning algorithm developed by Tinggaard in \cite{tinge2020} will be briefly explained.
            Tinggaard uses a quite different state representation compared to the one described in this paper consisting of two states describing the current position of a token, three states describing something that can happen after the dice roll and a state describing the number of enemies in the vicinity behind the token.
            The state is represented as a vector of integers, while the one presented in this paper is represented as a vector of booleans.
            To guide the algorithm to desired behaviours rewards are given throughout the training of the Q-table.
            These rewards are the ones that can be developed using a genetic algorithm or rather just a good guess.
            A Q-table consists of states on one axis and actions on the other.
            The Q-table is generated by playing the game many times and updating the values according to the Q-learning update rule. 
            

    \section{Results}\label{sec:results}
        In this section the developed ANN+GA player is playing against the original ANN, the random, the semismart and the Q-player\cite{tinge2020}, respectively.
        The game is set up such that two players of one type plays against two players of the other type.
        In each experiment $n = 10$ runs of 100,000 games are conducted.
        For each run the number of wins for each player is noted.
        A mean and a standard deviation of the 10 experiments for each player are calculated and these are compared.
        % Two of the same players are placed opposite on the board such that they are playing against the other player and not against itself.
        Two of the same players are placed opposite on the board such that the order of the players are balanced, ie. a player hunting a player of its own kind is avoided.

        \subsection{2 ANN+GA vs. 2 ANN}
            The results of this experiment can be seen in \autoref{tab:ann_ga_vs_ann} and graphically on \autoref{fig:against_ann}.
            Each of the two ANN+GA players have on average a win rate of $37.75 \pm 0.40 \%$.
            Each of the two original ANN players have on average a win rate of $12.25 \pm 0.10 \%$.

        \subsection{2 ANN+GA vs. 2 Random}
            The results of this experiment can be seen in \autoref{tab:ann_ga_vs_random} and graphically on \autoref{fig:against_random}.
            Each of the two ANN+GA players have on average a win rate of $42.21 \pm 0.37 \%$.
            Each of the two random players have on average a win rate of $7.79 \pm 0.08 \%$.

        \begin{table}[!tbh]
            \centerfloat
            \begin{minipage}{0.5\textwidth}
                \centering
                \caption{Results showing the win rate of the ANN+GA player vs. the original ANN player.}
                \label{tab:ann_ga_vs_ann}
                \begin{tabular}{l l l}
                    \toprule
                    Player 1 \& Player 3 & \qquad & Player 2 \& Player 4 \\
                    ANN+GA ($\mu \pm \sigma$) &  & ANN ($\mu \pm \sigma$) \\
                    \midrule
                    $37.75 \pm 0.4 \%$ & & $12.25 \pm 0.10 \%$ \\
                    \bottomrule
                \end{tabular}
            \end{minipage}\qquad
            \begin{minipage}{0.5\textwidth}
                \centering
                \caption{Results showing the win rate of the ANN+GA player vs. the random player.}
                \label{tab:ann_ga_vs_random}
                \begin{tabular}{l ll}
                    \toprule
                    Player 1 \& Player 3 & \qquad & Player 2 \& Player 4 \\
                    ANN+GA ($\mu \pm \sigma$) & & Random ($\mu \pm \sigma$) \\
                    \midrule
                    $42.21 \pm 0.37 \%$ & & $7.79 \pm 0.08 \%$ \\
                    \bottomrule
                \end{tabular}
            \end{minipage}\\
        \end{table}


        \subsection{2 ANN+GA vs. 2 Semismart}
            The results of this experiment can be seen in \autoref{tab:ann_ga_vs_semismart} and graphically on \autoref{fig:against_semismart}.
            Each of the two ANN+GA players have on average a win rate of $37.42 \pm 0.30 \%$.
            Each of the two semismart players have on average a win rate of $12.58 \pm 0.09 \%$.
        

        \subsection{2 ANN+GA vs. 2 Q-learning}           
            The results of this experiment can be seen in \autoref{tab:ann_ga_q_learning} and graphically on \autoref{fig:against_q}.
            Each of the two ANN+GA players have on average a win rate of $28.74 \pm 0.28 \%$.
            Each of the two Q-learning players have on average a win rate of $21.26 \pm 0.13 \%$. 
    

        \begin{table}[!tbh]
            \centerfloat
            \begin{minipage}{0.5\textwidth}
                \centering
                \caption{Results showing the win rate of the ANN+GA player vs. the semismart player.}
                \label{tab:ann_ga_vs_semismart}
                \begin{tabular}{lll}
                    \toprule
                    Player 1 \& Player 3 & \qquad & Player 2 \& Player 4 \\
                    ANN+GA ($\mu \pm \sigma$) &  & Semismart ($\mu \pm \sigma$) \\
                    \midrule
                    $37.42 \pm 0.30 \%$ & & $12.58 \pm 0.09 \%$ \\
                    \bottomrule
                \end{tabular}
            \end{minipage}\qquad
            \begin{minipage}{0.5\textwidth}
                \centering
                \caption{Results showing the win rate of the ANN+GA player vs. the Q-learning player\cite{tinge2020}.}
                \label{tab:ann_ga_q_learning}
                \begin{tabular}{l l l}
                    \toprule
                    Player 1 \& Player 3 & \qquad & Player 2 \& Player 4 \\
                    ANN+GA ($\mu \pm \sigma$) &  & Q-learning ($\mu \pm \sigma$) \\
                    \midrule
                    $28.74 \pm 0.28 \%$ & & $21.26 \pm 0.13 \%$ \\
                    \bottomrule
                \end{tabular}
            \end{minipage}
        \end{table}
        
        \begin{figure}[!tbh]
            \centerfloat
            \begin{minipage}{0.3\textwidth}
                \centering
                % \fbox{
                \includegraphics[trim={0.25cm 0.3cm 0.35cm 0}, clip, width=\textwidth]{win_data_against_ann.eps}
                % }
                \caption{Visualization of the data from \autoref{tab:ann_ga_vs_ann}.}
                \label{fig:against_ann}
            \end{minipage}\qquad
            \begin{minipage}{0.3\textwidth}
                \centering
                \includegraphics[trim={0.25cm 0.3cm 0.35cm 0}, clip, width=\textwidth]{win_data_against_random.eps}
                \caption{Visualization of the data from \autoref{tab:ann_ga_vs_random}.}
                \label{fig:against_random}
            \end{minipage}\qquad
            \begin{minipage}{0.3\textwidth}
                \centering
                \includegraphics[trim={0.25cm 0.3cm 0.35cm 0}, clip, width=\textwidth]{win_data_against_semismart.eps}
                \caption{Visualization of the data from \autoref{tab:ann_ga_vs_semismart}.}
                \label{fig:against_semismart}
            \end{minipage}\qquad
            \begin{minipage}{0.3\textwidth}
                \centering
                \includegraphics[trim={0.25cm 0.3cm 0.35cm 0}, clip, width=\textwidth]{win_data_against_q_learning.eps}
                \caption{Visualization of the data from \autoref{tab:ann_ga_q_learning}.}
                \label{fig:against_q}
            \end{minipage}\qquad
        \end{figure}
        

    \section{Analysis and Discussion}
        In this section the results from \autoref{sec:results} will be analysed.
        By looking at the results it is clear that the developed ANN+GA has a higher win rate compared to the four other players.
        Though to be thorough the results should be analysed statistically using a Student't t-test testing the following hypothesis

        \begin{itemize}
            \item[H$_0$:] Two independent samples have identical means, $\mu_1 = \mu_2$.
            \item[H$_1$:] Two independent samples does not have identical means, $\mu_1 \neq \mu_2$. 
        \end{itemize}

        This test assumes that the variance of the samples are similar.
        Looking at the results this can be assumed to be the case.
        The p-values for all the experiments were calculated and are displayed \autoref{tab:students_t}.
        All of the p-values are $< \alpha = 0.05$ and thus the H$_0$ is rejected for all of the experiments.
        This was expected as mentioned before by looking at the graphs.

        \begin{table}[!tbh]
            \centering
            \caption{Results of the Student's t-test.}
            \label{tab:students_t}
            \begin{tabular}{lll}
                \toprule
                Experiment & \qquad \qquad& p-value \\
                \midrule
                ANN+GA vs. ANN & & $< 0.001$ \\
                ANN+GA vs. Random & & $< 0.001$ \\
                ANN+GA vs. Semismart & & $< 0.001$ \\
                ANN+GA vs. Q-learning & & $< 0.001$ \\
                \bottomrule
            \end{tabular}
        \end{table}

        From \autoref{fig:against_ann} it is clear that the genetic algorithm indeed has improved the performance of the network.
        
        Looking at the results the ANN player and the Semismart player seems to do comparable against the ANN+GA player.
        The hypothesis that their means should be equal is tested with a Student's t-test, where it is found that the p-value is $<0.001$.
        Thus can that hypothesis be rejected.
    

    \section{Conclusion}

        % Christians kigger kun på states, der kigger på dens position i forhold til mål, stjerner, globus, osv. den kigger ikke på, hvordan andre brikker står, som min gør.
        In this paper a Ludo player designed using an ANN was designed.
        To improve on the performance of the player, a genetic algorithm was used to evolutionarily develop the network.
        The ANN+GA wins significantly more than the original ANN.
        The story is the same against the random player and the semismart player.
        Against Tinggaard's Q-learning player \cite{tinge2020} the win rate of the ANN+GA player is lower compared to the rest of the experiments, but it still wins.

        In his paper Tinggaard arguments that a reason for the lower performance of the Q-learning player could be that the state of the game only considers one token in relation to goal, stars and globes, it doesn't consider how the other tokens are placed.
        The ANN presented in this paper has all the states of the four tokens as the input.
        This way the ANN might learn some synergy effects between the tokens.
        This is something that could be investigated in a future work.

        To improve on the performance of this ANN a more detailed fitness function could be investigated. 
        Such a fitness function could include information about e.g. location of other player's tokens or consider the number of steps moved.
        The number of states could also be investigated. 
        Maybe some of them doesn't contribute a lot to the performance.

        In a future work it could also be interesting to experiment with a state representation that doesn't consider the specific number of a token but rather the order of which they're in on the board.
        This way an AI might be able to learn whether it's better to keep all the tokens close to each other or if it's most effective to only move with the first one.

        Overall it can be concluded that the ANN+GA Ludo player developed in this paper performs well playing against four other players; ANN, random, semismart and Q-learning.



    \section{Acknowledgements}
        Thanks to Poramate Manoonpong for interesting and very inspiring classes.
        Thanks to Dohnbæk for developing an improved Ludo game implementation without bugs.
        Thanks to Christian Tinggaard for his Ludo player developed using Q-learning with which the player developed in this paper was compared.

    \bibliographystyle{splncs03}
    \bibliography{myref}
\end{document}