#!/usr/bin/env python3

import numpy as np

def save_avg(fName):
    data = np.zeros((500,3))

    for i in range(3):
        raw = np.loadtxt(f'{fName}{i + 1}.txt', skiprows=0, delimiter=',')

        data[:, 0] = raw[:, 0]
        data[:, 1] += raw[:, 1]
        data[:, 2] = raw[:, 2]

    data[:, 1] /= 3

    np.savetxt(f"{fName}_avg.txt", data, delimiter=',')
    print (data)


if __name__ == "__main__":
    save_avg("mse_batch")
    save_avg("mse_incremental")
    save_avg("mse_rprop")