#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

file_names = ["win_data_against_q_learning", 
              "win_data_against_semismart",
              "win_data_against_random",
              "win_data_against_ann"]

players = {"win_data_against_q_learning" : "Q-learning", 
           "win_data_against_semismart" : "Semismart",
           "win_data_against_random" : "Random",
           "win_data_against_ann" : "ANN"}

ann_semismart_data = []

for name in file_names:
    data = np.loadtxt(f"{name}.txt", delimiter=",", skiprows=1)
    games = np.sum(data[0, :])
    print(data)

    mean = np.mean(data, axis=0)
    std = np.std(data, axis=0)

    print(mean)
    print(std)

    player_ann_data = 100 * np.concatenate((data[:, 0], data[:, 2])) / games
    print(player_ann_data)
    player_q_data = 100 * np.concatenate((data[:, 1], data[:, 3])) / games

    mean_ann = np.mean(player_ann_data)
    std_ann = np.std(player_ann_data)

    mean_q = np.mean(player_q_data)
    std_q = np.std(player_q_data)

    t, p = stats.ttest_ind(player_ann_data, player_q_data)
    # print(t, p)

    print('--------------')
    print(f'ANN player (\mu \pm \sigma) & {players[name]} (\mu \pm \sigma) \\\\')
    print(f'{mean_ann} \pm {std_ann} & {mean_q} \pm {std_q}')
    print('--------------')
    print(f'student t-test, p-value: {p}')

    plt.figure(figsize=(2,3), dpi=70)
    x = [0, 1]
    plt.bar(x, [mean_ann, mean_q], yerr=[std_ann, std_q], 
        color=["tab:blue", "tab:orange"], edgecolor="k")
    plt.xticks(x, ('ANN+GA', players[name]), rotation=10)

    plt.ylim((0, 45))
    plt.ylabel("Win rate [\%]")
    plt.tight_layout()

    plt.savefig(f"../pictures/{name}.eps")

    if players[name] == "ANN" or players[name] == "Semismart":
        ann_semismart_data.append(list(player_q_data))

ann_semismart_data = np.asarray(ann_semismart_data).T
print(ann_semismart_data)
## comparison of ann and semismart

t, p = stats.ttest_ind(ann_semismart_data[:, 0], ann_semismart_data[:, 1])
print(f'student t-test for ann vs semismart, p-value: {p}')

plt.show()