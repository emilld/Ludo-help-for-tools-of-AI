#include "fann.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

void load_data_set(std::ifstream & datafile,
                   std::vector<std::vector<float>> & input, 
                   std::vector<std::vector<float>> & output,
                   const unsigned int num_input,
                   const unsigned int num_output)
{
    uint64_t state;
    int token;
    std::string line;

    if (datafile.is_open())
    {
        while (std::getline(datafile, line, ' '))
        {
            // std::cout << line << '\n';

            state = std::strtoll(line.c_str(), NULL, 10);
            // std::cout << state << std::endl;

            std::vector<float> state_vector;
            state_vector.resize(num_input);

            for (size_t i = 0; i < state_vector.size(); i++)
            {
                state_vector.at(i) = (state >> i) & 0x1;
                // state_vector.at(i) = state_vector.at(i) >> i;

                if (state_vector.at(i) == 0)
                    state_vector.at(i) = -1; // Change data to go from -1 to 1
            }

            input.push_back(state_vector);

            std::getline(datafile, line);
            // std::cout << line << '\n';
            token = std::stoi(line);
            
            std::vector<float> token_select; // Output vector
            token_select.resize(num_output); 
            
            std::fill(token_select.begin(), token_select.end(), 0);

            token_select.at(token) = 1;

            for (auto & i : token_select)
                if (i == 0)
                    i = -1; // Data from -1 to 1             

            output.push_back(token_select);
        }
        datafile.close();
    }
}

int FANN_API test_callback(struct fann *ann, struct fann_train_data *train,
                           unsigned int max_epochs, unsigned int epochs_between_reports,
                           float desired_error, unsigned int epochs)
{
    printf("Epochs     %8d. MSE: %.5f. Desired-MSE: %.5f\n", epochs, fann_get_MSE(ann), desired_error);
    // std::ofstream datafile;
    // datafile.open("../output/mse_incremental3.txt", std::ios::out | std::ios::app | std::ios::binary);
    // datafile << epochs << ", " << fann_get_MSE(ann) << ", " << desired_error << std::endl;
    return 0;
}

int main()
{
    const unsigned int num_input = 60;
    const unsigned int num_output = 4;
    const unsigned int num_layers = 3;
    // const unsigned int num_neurons_hidden = (num_input + num_output) / 2;
    const unsigned int num_neurons_hidden = 6;
    const float desired_error = (const float) 0.01;
    const unsigned int max_epochs = 500;
    const unsigned int epochs_between_reports = 1;
    const float connection_rate = 0.5;

    struct fann *ann = fann_create_standard(num_layers, num_input,
        num_neurons_hidden, num_output);
    // struct fann *ann = fann_create_sparse(connection_rate, num_layers, num_input,
    //      num_neurons_hidden, num_output);

    // fann_set_activation_function_hidden(ann, FANN_SIGMOID);
    // fann_set_activation_function_output(ann, FANN_SIGMOID);
    fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
    fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC); // -1 to 1

    fann_randomize_weights(ann, -0.77, 0.77);
    fann_set_learning_rate(ann, 0.7);
    // fann_set_training_algorithm(ann, FANN_TRAIN_INCREMENTAL);
    fann_set_training_algorithm(ann, FANN_TRAIN_RPROP);
    // fann_set_training_algorithm(ann, FANN_TRAIN_BATCH);
    // fann_train_on_file(ann, "xor.data", max_epochs,
    //     epochs_between_reports, desired_error);

    std::ifstream training_set("../data/training_set_02.txt");
    std::ifstream validation_set("../data/validation_set_02.txt");

    std::vector<std::vector<float>> train_input_data;
    std::vector<std::vector<float>> train_output_data;

    std::vector<std::vector<float>> validate_input_data;
    std::vector<std::vector<float>> validate_output_data;

    load_data_set(training_set, train_input_data, train_output_data, num_input, num_output);    
    load_data_set(validation_set, validate_input_data, validate_output_data, num_input, num_output);    
    
    for (auto line : train_input_data)
    {
        for (auto state : line)
            std::cout << state;
        std::cout << std::endl;
    }

    for (auto tokens : train_output_data)
    {
        for (auto token : tokens)
            std::cout << token;
        std::cout << std::endl;
    }

    fann_train_data* train_data = fann_create_train((int) train_input_data.size(), num_input, num_output);
    fann_train_data* validate_data = fann_create_train((int) validate_input_data.size(), num_input, num_output);
    // for (size_t i = 0; i < input_data.size(); i++)
    // {
    //     fann_train(ann, &input_data.at(i)[0], &output_data.at(i)[0]);
    // }

    for (size_t i = 0; i < train_input_data.size(); i++)
    {
        train_data->input[i] = &train_input_data.at(i)[0];
        train_data->output[i] = &train_output_data.at(i)[0];
        // std::cout << *train_data->output[i] << std::endl;
    }
    
    for (size_t i = 0; i < validate_input_data.size(); i++)
    {
        validate_data->input[i] = &validate_input_data.at(i)[0];
        validate_data->output[i] = &validate_output_data.at(i)[0];
    }

    std::cout << fann_get_total_neurons(ann) << std::endl;
    std::cout << fann_get_total_connections(ann) << std::endl;

    // return 0;

    // std::cout << train_data->input[0] << std::endl;
    fann_set_callback(ann, test_callback);
    fann_train_on_data(ann, train_data, max_epochs, epochs_between_reports, desired_error);
    
    float mse = fann_test_data(ann, validate_data);
    std::cout << "MSE on validation set: " << mse << std::endl;

    mse = fann_test_data(ann, train_data);
    std::cout << "MSE on training set: " << mse << std::endl;

    fann_save(ann, "../ann_ludo_testing.net");

    fann_destroy(ann);

    return 0;
}