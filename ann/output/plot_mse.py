#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

data_batch = np.loadtxt("mse.txt", delimiter=",", skiprows=0)
data_rprop = np.loadtxt("mse_rprop.txt", delimiter=",", skiprows=0)
data_incremental = np.loadtxt("mse_incremental.txt", delimiter=",", skiprows=0)

# print(data)

plt.figure()
plt.plot(data_batch[:, 0], data_batch[:, 1], label="Batch")
plt.plot(data_rprop[:, 0], data_rprop[:, 1], label="RPROP")
plt.plot(data_incremental[:, 0], data_incremental[:, 1], label="Incremental")

plt.legend()
plt.show()
