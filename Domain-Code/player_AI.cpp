#include "player_AI.h"
#include "iostream"
#include "fstream"
#include "string"
#include "sstream"
#include <bits/stdc++.h> 


using namespace std;

player_AI::player_AI(int stateLength,bool toTrain,vector<float> dna,string qname)
{
    lengthOfState=stateLength;
    //eGreedy=e;
    train=toTrain;
    QName=qname;
    

    learningRate=dna.at(0);
    eGreedy=dna.at(1);
    rewardReturnPiece=dna.at(2);
    rewardPieceCantGetOut=dna.at(3);
    rewardAtGlobe=dna.at(4);
    rewardMovedPieceOutOfHome=dna.at(5);
}


     


int player_AI::make_decision()
{

    

    int highestQVal=-1000;
    int index=0; 

    for (size_t i = 0; i < 4; i++)
    {
        if(position[i]!=99)
        {
            break;
        }
        if (i==3)
        {
            cout << "you won?" << endl;
        }
        
    }
    
    

    // Start by updating the reward for the last move taken, dont do this the first time!
    if (lastPickedPiece>=0 || (pickedPiecePosition==99 && position[lastPickedPiece]==-1 ))
    {
        float reward=position[lastPickedPiece]-pickedPiecePosition;
        if (position[lastPickedPiece]==0 && pickedPiecePosition==-1)
        {
            // reward+=30;
            reward+=rewardMovedPieceOutOfHome;
            //cout << "piece out " << endl;
        }
        
        if (returnedAPiece)
        {
            
            //reward+=30;
            reward+=rewardReturnPiece;
        }

        if (position[lastPickedPiece]==-1&&pickedPiecePosition==-1)
        {
            //reward-=100; // if picking a piece that cannot get out 
            reward+=rewardPieceCantGetOut;
        }
        
        if (lastState.size()>3)
        {
            if (lastState.at(3))
            {
                //reward-=10;
                reward+=rewardAtGlobe;
            }
            
            
        }
        if (pickedPiecePosition == 99  && 99 == position[lastPickedPiece])
        {
            //cout << "Moved a piece which was already at home"<< endl;
            //reward-=100;
        }
        
        
        
        
        //cout << "Last position: " << pickedPiecePosition << " New position" << position[lastPickedPiece] <<  " Last dice: " << lastState.at(0) << endl;
        if (train)
        {
            vector<int> currState = createState(lastPickedPiece);
            updateReward(lastState,currState,reward);
        }
        
        
        //cout << "diff:  " << pickedPiecePosition - position[lastPickedPiece] << endl;


        
    }
    
    

    // Now calculate best piece to move, but only if were greedy, else take a random move

    distributionF = std::uniform_int_distribution<int>(0, 9);
    vector<int>bestState;
    if (distributionF(generator)<eGreedy*10) 
    {
        // Greedy
        
        for (size_t i = 0; i < 4; i++)
        {
            if (position[i]!=1000)
            {
                
                vector<int> state=createState(i);
                float Qvalue=findQVal(state);
                
                if (highestQVal<Qvalue)
                {
                    highestQVal=Qvalue;
                    index=i;
                    bestState=state;
                } 
            }
            
            
            
        }
    }
    else
    {
        
        // Random
        distributionI = std::uniform_int_distribution<int>(0, 3);
        int i=distributionI(generator);
        vector<int> state=createState(i);
        float Qvalue=findQVal(state);
        highestQVal=Qvalue;
        index=i;
        bestState=state;

    }



    
    if (canReturn(position[index]+dice)&& position[index]!=-1)
    {
        returnedAPiece=1;
    }
    else
    {
        returnedAPiece=0;
    }
    
    
    //cout << "Index: "<< index << " " << "Pos: "<< position[index] << endl;
    // Update variables for next round

    lastState=bestState;
    lastPickedPiece=index;
    
    pickedPiecePosition=position[index];
    //cout << "dice" << dice << endl;
    //cout << Qvalues.size() << endl;
    return index;
}

vector<int> player_AI::createState(int piece)
{   
    
    int starORglobeORout =0;
    int pos=-1;
    int canRet=0;
    int newPosition;
    int standingOnGlobe=0;
    int canGetToGoal = 0;

    if (position[piece]==-1)
    {
        newPosition=0;
        if (dice==6)
        {
            starORglobeORout=3;
        }
        
    }
    else
    {
        newPosition = position[piece]+dice;    
    }
    
    

    if (isStar(newPosition))
    {
        
        
        newPosition+=6; // The least amount of steps moved when landing on a star

        switch (newPosition)
        {
        case 17: 
            newPosition++; // add one ekstra for some of the stars
            break;
        case 30: 
            newPosition++; // add one ekstra for some of the stars
            break;
        case 43: 
            newPosition++; // add one ekstra for some of the stars
            break;
        
        default:
            break;
        }
        
        
        starORglobeORout=1;
    }
    else if (isGlobe(newPosition))
    {
        starORglobeORout=2;
    }
    
    


    
    if (position[piece]!=-1)
    {
        pos=floor(position[piece]/10);
    }
    else
    {
        pos=-1;
    }
    
    if (pos==5)
    {
        if (newPosition==57)
        {
            canGetToGoal=1;
        }
        else if (newPosition>57)
        {
            canGetToGoal=-1; //going past goal
        }
        
        
        
    }
    else
    {
        if (canReturn(newPosition))
        { 
            canRet=1; // correct
        

        }
    }
    
    
    
        
    

    int enemies=enemiesBehind(piece);

    if (isGlobe(position[piece]))
    {
        standingOnGlobe=1;
    }
    

    // kan lande på ven
    // hvor mange modstandere er i mål
    // 
    
    //vector<int> state ={pos,starORglobeORout,canRet,standingOnGlobe,enemies,canGetToGoal};
    vector<int> state ={pos,starORglobeORout,canRet,standingOnGlobe,canGetToGoal};



    //vector<int> state ={pos,starORglobeORout,canRet,standingOnGlobe,enemies};
    //vector<int> state ={pos,starORglobeORout,canRet,standingOnGlobe};
    vector<int> out;

    for (size_t i = 0; i < lengthOfState; i++)
    {
        if (i==state.size())
        {
            break;
        }
        
        out.push_back(state[i]);
    }
    
    return out;
    
    /*

    position= pos&10

    if (is_star(position[piece]) )
    {
         
    }
    if (is_globe(position[piece]))
    {

    }
    
      
    */
}

float player_AI::findQVal(vector<int> state)
{
    float mapReturn = Qvalues[state];
    if (mapReturn)
    {
        return mapReturn; // Key already exist, return the value
    }
    else
    {
        // Key does not exist, create key and return default value
        Qvalues[state]=defaultMapReturn;
        return defaultMapReturn;
    }
    
    
    
}

void player_AI::updateReward(vector<int> laststate ,vector<int> currState, float reward)
{
    // Look at last location of piece and new location of the same piece. 

    // float Qlast=Qvalues[lastState];
    // float Qnow=Qvalues[currState];
    
    float Qlast=findQVal(lastState);
    float Qnow=findQVal(currState);
    

    //Qvalues[lastState]=Qlast+learningRate*(reward+discount*Qnow-Qlast);
    Qvalues[lastState]=Qlast+learningRate*(reward-Qlast);
    
    /*
    cout << "Reward: " << reward << endl;
    cout << "LastQ: " << Q << endl;
    */
}




bool player_AI::isStar(int field)
{
    switch (field)
    {
    case 5:
        return true;
    case 11:
        return true;
    case 18:
        return true;
    case 24:
        return true;
    case 31:
        return true;
    case 37:
        return true;
    case 44:
        return true;
    case 50:
        return true;
            
    
    default:
        return false;;
    }
    
}
bool player_AI::isGlobe(int field)
{
    switch (field)
    {
    case 8:
        return true;
    case 21:
        return true;
    case 34:
        return true;
    case 47:
        return true;
    // Home:
    // case 0:
    //     return true;
    // case 13:
    //     return true;        
    // case 26:
    //     return true;
    // case 39:
    //     return true;
    
    default:
        return false;;
    }
}

bool player_AI::canReturn(int field)
{
    for (size_t i = 4; i < 16; i++)
    {
        if (field==position[i])
        {
            
            if ((field == int(floor(i/4)*13)))
            {
                return false;
            }
            
            for (size_t j = floor(i/4)*4; j < (floor(i/4)*4)+4; j++)
            {
                if (j!=i && position[j]==field)
                {
                    return false;
                }
                
            }
            if (i==15)
            {
                return true;
            }
                        
        }
        
    }
    return false;
    
}

int player_AI::enemiesBehind(int piece)
{
    
    int enemies=0;
    
    for (size_t i = 4; i < 16; i++)
    {
        if (position[piece]<50)
        {
            if (position[i]<position[piece]&&position[i]>(position[piece]-backwardsVision)%51 && position[i]!=-1)
            {
            enemies++;
            }
        }
        
        
        
    
    }
    return enemies;

}

void player_AI::saveMap()
{
    // save a map
    ofstream file;
    stringstream ss;
    ss << "../Qval/QValues" << QName << ".csv"; 
    string fileName=ss.str();
    file.open(fileName,ios::out | ios::trunc);


     for(map<vector<int>, float>::iterator it = Qvalues.begin(); it != Qvalues.end(); it++) {
         for (size_t i = 0; i < it->first.size(); i++)
         {
             //cout << it->first.at(i) << endl;
             file <<  it->first.at(i) << "," ;
         }
         
            file << it->second << "\n";
                
        }



    


    file.close();

}


void player_AI::loadMap()
{
    // load a map
    string line;
    
    ifstream file;
    float c;
    
    stringstream ss;
    // ss << "../Qval/QValues" << QName << ".csv"; 
    ss << "../QValues" << QName << ".csv";
    string fileName=ss.str();

    file.open(fileName,ios::in);
    
    if (file.fail())
    {
        //cout << "file does not exist" << endl;
        return;
    }
    //cout << "Loading file" << endl;
    
    
    while (std::getline(file,line))
    {
        
        float reward;
        vector<int> state;
        
        
        string tmp="";



        for (size_t i = 0; i < line.size(); i++)
        {
            

            if (line.at(i)!=',' )
            {
                //cout << tmp << endl;
                tmp.push_back(line.at(i));
                if (i+1==line.size())
                {
                    /* code */
                }
                else
                {
                    continue;
                }
                
                
                
            }

            if (i+1==line.size())
            {
                reward=stod(tmp);
                
            }
            else
            {   
                
                //cout << stof(tmp) << endl;
                

                state.push_back(stod(tmp));
                tmp="";

                
                
            }
            
            
            
            
        }
        
        // cout << endl;
        // cout << reward << endl;
        Qvalues[state]=reward;
        
    }
    

    
    


    file.close();
    return;
}