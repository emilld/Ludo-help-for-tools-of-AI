#ifndef GA_TRAINER_H
#define GA_TRAINER_H

#include "player_semismart.h"
#include "player_random.h"
#include "player_ann.h"
#include <vector>
#include <string>

#include "game.h"

struct Individual
{
    std::vector<float> weights;
    float fitness; // Number of wins
};

class ga_trainer
{
private:    
    int games_per_evaluation;

    int population_size;
    std::vector<Individual> population;
    std::vector<Individual> new_population;

    void initialize_population();
    
    int generation;

    void evaluate_player(Individual & genom);
    int evaluation_thread(Individual & genom, int games);

    template <class T>
    float dist(std::vector<T> arr1, std::vector<T> arr2);

    void evaluate_player2(Individual & genom);
    float evaluation_thread2(Individual & genom, int games);

    void mutate_genom(std::vector<float>& genom, float mean = 0.0, float std = 1.0, float chance = 0.1);

    void select_and_reproduce();
    void select_random(std::vector<Individual> & population, 
                       std::vector<Individual> & new_population, 
                       int s);
    void select_tournament(std::vector<Individual> & population, 
                           std::vector<Individual> & new_population, 
                           int s);
    std::vector<int> tournament_game(std::vector<float> & genom1,
                                     std::vector<float> & genom2, 
                                     std::vector<float> & genom3,
                                     std::vector<float> & genom4,
                                     int games);
    std::vector<int> tournament_thread(std::vector<float> & genom1,
                                       std::vector<float> & genom2, 
                                       std::vector<float> & genom3,
                                       std::vector<float> & genom4,
                                       int games);

    std::vector<float> crossover(const std::vector<float> & genom1,
                                 const std::vector<float> & genom2);

    std::default_random_engine generator;

    int no_of_threads;

    float ELITISM, MUTATION_RATE;
    int TOURNAMENT_GAMES;

public:
    ga_trainer(int population_size, int evaluation_games = 400, int no_of_threads = 16);
    ~ga_trainer();

    void generation_loop();
    void save_population();
    void write_description_file(std::string & filename);

    void set_elitism(float val)
    { // Between 0 and 1. How many to save to next iteration
        ELITISM = val * 100;
    }

    void set_mutation_rate(float val)
    {
        MUTATION_RATE = val;
    }

    void set_tournament_games(int games)
    {
        TOURNAMENT_GAMES = games;
    }
};


#endif // GA_TRAINER_H
