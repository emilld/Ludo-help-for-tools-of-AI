cmake_minimum_required (VERSION 3.1)
project (ludo)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# Local header files here ONLY
SET(TARGET_H
    dice.h
	game.h
	player_random.h
	positions_and_dice.h
	test_game.h
	iplayer.h
	player_semismart.h
	player_ann.h
	ga_trainer.h
	player_ann_ga.h
	player_AI.h 
   )

# Local source files here
SET(TARGET_SRC
    game.cpp
	test_game.cpp
	main.cpp
	player_semismart.cpp
	player_ann.cpp
	ga_trainer.cpp
	player_AI.cpp
)

add_executable(ludo ${TARGET_SRC})

find_library(FANN_LIB fann)

TARGET_LINK_LIBRARIES(ludo "${FANN_LIB}")

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(ludo Threads::Threads)