#ifndef PLAYER_SEMISMART_H
#define PLAYER_SEMISMART_H

#include "iplayer.h"

class player_semismart : public iplayer 
{
    private:
        int make_decision();
};

#endif // PLAYER_SEMISMART_H
