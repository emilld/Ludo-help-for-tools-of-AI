#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

top_names = ["10% elitism, 10% mutation",
             "10% elitism, 50% mutation",
             "25% elitism, 10% mutation",
             "25% elitism, 50% mutation"]

top_names_labels = ["10\% elitism, 10\% mutation",
                    "10\% elitism, 50\% mutation",
                    "25\% elitism, 10\% mutation",
                    "25\% elitism, 50\% mutation"]

inner_names = ["01", "02", "03"]

linestyle = ["-", "--", "-.", ":"]

plt.figure(figsize=(5,3), dpi=70)

i = 0

for top_name in top_names:
    # plt.figure()
    # plt.title(top_name)
    data = []
    for inner_name in inner_names:
        # with open(f'{top_name}/{inner_name}/progress.txt') as f:
            # print (f.readline())
        tmp = np.loadtxt(f'{top_name}/{inner_name}/progress.txt', delimiter=",")
        data.append(tmp[:, 1])

    data = np.asarray(data)  
    print(data)
    print(data.shape)

    mean = np.mean(data, axis=0)
    std = np.std(data, axis=0)

    # print(f'{mean} +- {std}')

    # plt.plot(data[:, 0], data[:, 1], label=f'{top_name}/{inner_name} max')
    print(range(data.shape[1]), data[:, 0])
    # plt.errorbar(np.arange(0, mean.size, 1), mean, yerr=std, alpha=0.5, label=f'{top_name}')
    plt.plot(mean, label=f'{top_names_labels[i]}', alpha=1, linestyle = linestyle[i])
    i += 1

plt.xlabel("Geneation")
plt.ylabel("Fitness")
plt.legend()
plt.grid()
plt.tight_layout()

plt.savefig("generations_comparison.eps")
plt.show()