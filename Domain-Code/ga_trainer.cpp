#include "ga_trainer.h"

#include "game.h"
#include <vector>

#include <iterator>
#include <random>
#include <chrono>
#include <fstream>

#include <algorithm>

#include <thread>
#include <future>
#include <iomanip>
#include <sstream>

// #include <assert.h>  

ga_trainer::ga_trainer(int num, int evaluation_games, int _no_of_threads)
{
    population_size = num;

    unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    generator.seed(seed1);

    games_per_evaluation = evaluation_games;
    generation = 0;
    initialize_population();

    // std::sort(population.begin(), population.end());
    // std::reverse(population.begin(), population.end());

    no_of_threads = _no_of_threads;

    set_elitism(0.1);
    set_mutation_rate(0.1);
    set_tournament_games(100);
}

ga_trainer::~ga_trainer()
{
}

void ga_trainer::write_description_file(std::string & filename)
{
    std::ofstream file(filename);
    std::cout << "Writing description to file " << filename << std::endl;
    file << "Evaluation of genom against three of the original ANN players." << std::endl;
    file << (int) ELITISM << "\% elitism." << std::endl;
    file << "50\% crossover" << std::endl; 
    file << (int) (MUTATION_RATE * 100) << "\% mutation with std = 2." << std::endl;
    file << games_per_evaluation << " games per evalation of each of the " << population_size << " genom." << std::endl;
    file << "Tournament selection" << std::endl;
    file << "Tournament games: " << TOURNAMENT_GAMES << std::endl;
    file << "Number of hidden neurons: " << (new player_ann)->get_num_hidden() << std::endl;
    file.close();
}

void ga_trainer::save_population()
{
    std::ostringstream filename;
    filename << "../output/generations/generation";
    filename << std::setfill('0') << std::setw(5) << generation;
    filename << ".csv";

    std::ofstream outfile(filename.str());

    for (Individual & i : population)
    {
        for (auto & w : i.weights)
        {
            outfile << w << ", ";
        }
        outfile << i.fitness << std::endl;
    }

    outfile.close();
}

void ga_trainer::initialize_population()
{
    // Get the original weights:
    player_ann p4;
    std::vector<float> weights = p4.get_ann_weights();

    // Make a random noise generator

    for (size_t i = 0; i < population_size; i++)
    {
        Individual tmp;
        tmp.weights = weights;
        tmp.fitness = 0;
        population.push_back(tmp);

        mutate_genom(population.back().weights, 0, 1, 1);
    }
    // for (auto & genom : population)
    // {
    //     for (auto & gene : genom)
    //         std::cout << gene << "; ";
        
    //     std::cout << std::endl;
    // }

    // std::cout << population.size() << std::endl;
}

void ga_trainer::mutate_genom(std::vector<float> & genom, float mean, float std, float chance)
{
    std::uniform_int_distribution<> dis(0, 100);
    std::normal_distribution<double> distribution(mean, std);
    
    for (auto & x : genom)
        if (dis(generator) < chance * 100)
            x += distribution(generator);
}

void ga_trainer::generation_loop()
{    
    // Go through each genom and evaluate it to get a score
    int k = 0;
    for (auto & genom : population)
    {
        // std::cout << "Start of loop" << std::endl;

        evaluate_player(genom);

        std::cout << "Individual " << k << "; Wins: " << genom.fitness << std::endl;
        k++;

        // std::cout << "End of loop" << std::endl;
    }

    std::sort(population.begin(), population.end());
    std::reverse(population.begin(), population.end());
    for (auto & i : population)
        std::cout << i.fitness << std::endl;

    // After this the current population has been evaluated.
    // Now the best should be selected.
    // for (auto & gen : population)
    //     std::cout << gen.fitness << std::endl;

    float mean = 0;

    for (Individual & i : population)
        mean += i.fitness;

    mean /= population_size;

    std::cout << "Generation: " << generation << ", Fittest: " << population.at(0).fitness << ", Mean: " << mean << std::endl;
    std::cout << "Population size: " << population.size() << std::endl;

    std::ofstream file("../output/progress.txt", std::ios::app);
    file << generation << ", " << population.at(0).fitness << ", " << mean << ", " << population.back().fitness << std::endl;
    file.close();

    std::cout << "Saving population" << std::endl;
    save_population();

    std::cout << "Selecting and reproducing" << std::endl;
    select_and_reproduce();

    population = new_population;
    new_population.clear();

    generation++;
}

bool operator<(const Individual & ind1, const Individual & ind2)
{
    return ind1.fitness < ind2.fitness;
}

template<typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename Iter>
Iter select_randomly(Iter start, Iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}

void ga_trainer::select_and_reproduce()
{
    new_population.clear();
    // Select the k most fittest genoms for reproduction
    // Tournament to find parents to make the new population.
    std::sort(population.begin(), population.end());
    std::reverse(population.begin(), population.end());

    // Elitism. Save the best 10% of the population.
    int s = ELITISM * population_size / 100;

    for (size_t i = 0; i < s; i++)
        new_population.push_back(population.at(i));     
    
    select_tournament(population, new_population, s);

    // std::uniform_int_distribution<> distribution(0, (int) population_size/2);

    // for (size_t i = 0; i < population_size - s; i++)
    // {
    //     // Find 2 random parents in the 50% of the population and cross them over
    //     id1 = distribution(generator); 
    //     Individual parent1 = population.at(id1);

    //     id2 = distribution(generator); 
    //     Individual parent2 = population.at(id2);

    //     std::cout << "Parent 1: " << parent1.fitness << ", Parent 2: " << parent2.fitness << std::endl;

    //     // Crossover the two best to create a baby and push baby into the new population
    //     std::vector<float> new_genom = crossover(parent1.weights, parent2.weights);
    //     mutate_genom(new_genom, 0, 2, MUTATION_RATE);
    //     Individual new_g;
    //     new_g.weights = new_genom;
    //     new_g.fitness = 0;
    //     new_population.push_back(new_g);
    // }
}

void ga_trainer::select_random(std::vector<Individual> & population, 
    std::vector<Individual> & new_population, 
    int s)
{
    int id1;
    int id2;
    std::uniform_int_distribution<> distribution(0, (int) population_size/2);

    for (size_t i = 0; i < population_size - s; i++)
    {
        // Find 2 random parents in the 50% of the population and cross them over
        id1 = distribution(generator); 
        Individual parent1 = population.at(id1);

        id2 = distribution(generator); 
        Individual parent2 = population.at(id2);

        std::cout << "Parent 1: " << parent1.fitness << ", Parent 2: " << parent2.fitness << std::endl;

        // Crossover the two best to create a baby and push baby into the new population
        std::vector<float> new_genom = crossover(parent1.weights, parent2.weights);
        mutate_genom(new_genom, 0, 2, MUTATION_RATE);
        Individual new_g;
        new_g.weights = new_genom;
        new_g.fitness = 0;
        new_population.push_back(new_g);
    }   
}

void ga_trainer::select_tournament(std::vector<Individual> & population, 
                                   std::vector<Individual> & new_population, 
                                   int s)
{
    int id1;
    int id2;
    std::uniform_int_distribution<> distribution(0, (int) population_size/2);

    for (size_t i = 0; i < population_size - s; i++)
    {
        // Find 2 random parents in the 50% of the population and cross them over
        // id1 = distribution(generator); 
        // Individual parent1 = population.at(id1);

        // id2 = distribution(generator); 
        // Individual parent2 = population.at(id2);
        std::vector<int> contestors;
        for (size_t i = 0; i < 4; i++)
        {
            contestors.push_back(distribution(generator));
            // std::cout << contestors.at(i) << std::endl;
        }

        std::vector<int> parents = tournament_game(population.at(contestors.at(0)).weights,
                                                   population.at(contestors.at(1)).weights,
                                                   population.at(contestors.at(2)).weights,
                                                   population.at(contestors.at(3)).weights,
                                                   TOURNAMENT_GAMES);

        Individual parent1 = population.at(contestors.at(parents.at(0)));
        Individual parent2 = population.at(contestors.at(parents.at(1)));

        std::cout << "Parent 1: " << parent1.fitness << ", Parent 2: " << parent2.fitness << std::endl;

        // Crossover the two best to create a baby and push baby into the new population
        std::vector<float> new_genom = crossover(parent1.weights, parent2.weights);
        mutate_genom(new_genom, 0, 2, MUTATION_RATE);
        Individual new_g;
        new_g.weights = new_genom;
        new_g.fitness = 0;
        new_population.push_back(new_g);
    }
    // std::cout << "New population size: " << new_population.size() << std::endl;
}

std::vector<int> ga_trainer::tournament_game(std::vector<float> & genom1,
                                             std::vector<float> & genom2, 
                                             std::vector<float> & genom3,
                                             std::vector<float> & genom4,
                                             int games)
{
    // player_ann p1;
    // player_ann p2;
    // player_ann p3;
    // player_ann p4;
    
    // // std::cout << "Starting tournament" << std::endl;

    // p1.set_ann_weights(genom1);
    // p2.set_ann_weights(genom2);
    // p3.set_ann_weights(genom3);
    // p4.set_ann_weights(genom4);

    // game g(&p1, &p2, &p3, &p4);

    // std::vector<int> wins = {0, 0, 0, 0};

    // for (size_t i = 0; i < games; i++)
    // {
    //     // std::cout << i << std::endl;
    //     g.reset();
    //     g.set_first(i%4); //alternate who starts the game
    //     g.play_game();
    //     wins.at(g.get_winner())++;
    // }

    int games_per_thread = (int)games/no_of_threads;
    int residuals = games - games_per_thread * no_of_threads;

    std::vector<std::future<std::vector<int>>> thread_pool;

    for (size_t i = 0; i < no_of_threads; i++)
    {
        if (i != no_of_threads - 1)
        {
            thread_pool.push_back(
                std::async(
                    std::launch::async, &ga_trainer::tournament_thread, this, 
                        std::ref(genom1), std::ref(genom2), std::ref(genom3), std::ref(genom4), games_per_thread
                )
            );
        }
        else 
        {
            thread_pool.push_back(
                std::async(
                    std::launch::async, &ga_trainer::tournament_thread, this, 
                        std::ref(genom1), std::ref(genom2), std::ref(genom3), std::ref(genom4), games_per_thread + residuals 
                )
            );
        }
    }

    // std::vector<int> wins = tournament_thread(genom1, genom2, genom3, genom4, games);
    std::vector<int> wins = {0, 0, 0, 0};
    std::vector<int> temp;

    // std::cout << "Start of reading threads" << std::endl;

    for (size_t i = 0; i < no_of_threads; i++)
    {
        temp = thread_pool.at(i).get();
        // for (auto & x : temp)
        //     std::cout << x << ", ";
        // std::cout << std::endl;

        for (size_t j = 0; j < wins.size(); j++)
        {
            wins.at(j) += temp.at(j);
        }
    }

    // std::cout << "End of reading threads" << std::endl;

    // return the ids for the two best players
    int largest = -1;
    int largest_ID;
    int second = -1;
    int second_ID;

    int e;
    for (size_t i = 0; i < 4; i++)
    {
        e = wins.at(i);
        if (e > largest)
        {
            second = largest;
            second_ID = largest_ID;
            largest = e;
            largest_ID = i;
        }
        else if (e > second)
        {
            second = e;
            second_ID = i;
        }
    }

    std::vector<int> out = {largest_ID, second_ID};
    // std::cout << out.at(0) << " " << out.at(1) << std::endl;
    return out;
}

std::vector<int> ga_trainer::tournament_thread(std::vector<float> & genom1,
                                               std::vector<float> & genom2, 
                                               std::vector<float> & genom3,
                                               std::vector<float> & genom4,
                                               int games)
{
    player_ann p1;
    player_ann p2;
    player_ann p3;
    player_ann p4;
    
    // std::cout << "Starting tournament" << std::endl;

    p1.set_ann_weights(genom1);
    p2.set_ann_weights(genom2);
    p3.set_ann_weights(genom3);
    p4.set_ann_weights(genom4);

    game g(&p1, &p2, &p3, &p4);

    std::vector<int> wins = {0, 0, 0, 0};

    for (size_t i = 0; i < games; i++)
    {
        // std::cout << i << std::endl;
        g.reset();
        g.set_first(i%4); //alternate who starts the game
        g.play_game();
        wins.at(g.get_winner())++;
    }

    return wins;
}

void ga_trainer::evaluate_player(Individual & genom)
{
    

    // for (int & j : winners)
    // {
    //     // std::cout << j << ", ";
    //     if (j == 3)
    //         wins++;
    // }
    // std::cout << std::endl;
    int games_per_thread = (int)games_per_evaluation/no_of_threads;
    int residuals = games_per_evaluation - games_per_thread * no_of_threads;
    // std::cout << games_per_thread << " " << residuals << std::endl;

    std::vector<std::future<int>> thread_pool;
    int games = 0;
    for (size_t i = 0; i < no_of_threads; i++)
    {
        if (i != no_of_threads - 1)
        {
            thread_pool.push_back(
                std::async(
                    std::launch::async, &ga_trainer::evaluation_thread, this, std::ref(genom), games_per_thread
                )
            );
            games += games_per_thread;
        }
        else
        {
            thread_pool.push_back(
                std::async(
                    std::launch::async, &ga_trainer::evaluation_thread, this, std::ref(genom), (int)games_per_thread + residuals
                )
            );
            games += games_per_thread + residuals;
        } 
    }
    
    int wins = 0;
    for (size_t i = 0; i < thread_pool.size(); i++)
    {
        wins += thread_pool.at(i).get();
    }
    
    genom.fitness = (float) wins / (float) games_per_evaluation;
    std::cout << "wins: " << wins << " games " << games << " fitness: " << genom.fitness << std::endl;
}

int ga_trainer::evaluation_thread(Individual & genom, int games)
{ // Evaluate one player by playing it against three other
    player_semismart p1;
    player_semismart p2;
    player_semismart p3;
    player_ann p4;

    p4.set_ann_weights(genom.weights);

    game g(&p1, &p2, &p3, &p4);

    int wins[] = {0, 0, 0, 0};

    for (size_t i = 0; i < games; i++)
    {
        // std::cout << i << std::endl;
        g.reset();
        g.set_first(i%4); //alternate who starts the game
        g.play_game();
        wins[g.get_winner()]++;
    }

    return wins[3];
}

std::vector<float> ga_trainer::crossover(const std::vector<float> & genom1, 
                                         const std::vector<float> & genom2)
{
    // Go through all genom and randomly take a value from one or the other
    std::vector<float> out;
    out.resize(genom1.size());

    std::random_device rd;
    std::uniform_int_distribution<int> distribution(1, 100);
    std::mt19937 engine(rd()); // Mersenne twister MT19937

    unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    engine.seed(seed1);

    int value;

    for (size_t i = 0; i < genom1.size(); i++)
    {
        value = distribution(engine);

        if (value > 50)
            out.at(i) = genom1.at(i);
        else
            out.at(i) = genom2.at(i);
    }
    
    return out;
}
