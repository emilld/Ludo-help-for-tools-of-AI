#ifndef PLAYER_ANN_H
#define PLAYER_ANN_H

#include <iostream>
#include <fstream>
#include <random>

#include "fann.h" // Fast Artificial Neural Network
#include "iplayer.h"

// Define states:
#define STATE_TOKEN_IS_FREE             1 <<  0
#define STATE_TOKEN_IS_DEAD             1 <<  1 
#define STATE_TOKEN_IS_ON_STAR          1 <<  2
#define STATE_TOKEN_IS_ON_GLOBE         1 <<  3
#define STATE_TOKEN_IS_ON_GOAL_STRETCH  1 <<  4
#define STATE_TOKEN_IS_FINISHED         1 <<  5
#define STATE_TOKEN_IS_ON_ENEMY_START   1 <<  6
#define STATE_TOKEN_IS_FORMING_BLOCKAGE 1 <<  7

#define STATE_TOKEN_CAN_FORM_BLOCKAGE   1 <<  8
#define STATE_TOKEN_CAN_KILL_ENEMY      1 <<  9
#define STATE_TOKEN_CAN_REACH_GOAL      1 << 10
#define STATE_TOKEN_CAN_LEAVE_HOME      1 << 11
#define STATE_TOKEN_CAN_GO_TO_STAR      1 << 12
#define STATE_TOKEN_CAN_GO_TO_GLOBE     1 << 13
#define STATE_TOKEN_CAN_DIE             1 << 14

#define NUM_OF_STATES                   15

class player_ann : public iplayer {
    private:
        int make_decision();
        int64_t get_state();

        // const unsigned num_layers = 3;
        // const unsigned num_input = 40;
        // const unsigned num_hidden = 5;
        // const unsigned num_output = 4;

        // struct fann *ann = fann_create_standard(num_layers, num_input, num_hidden, num_output); 
    protected:
        struct fann* ann = fann_create_from_file("../../ann/ann_ludo_6_hidden.net");
        // Randgen for last choice
        std::random_device rd; // obtain a random number from hardware
        std::mt19937 eng; // seed the generator

    public:
        player_ann();
        ~player_ann();
        std::vector<float> get_ann_weights();
        void set_ann_weights(std::vector<float>&);
        std::vector<int> get_position();
        
        int get_num_hidden();
};

#endif // LUDO_PLAYER_ANN_H
