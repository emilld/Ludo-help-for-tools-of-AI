#include "player_ann.h"
#include <random>
#include <iostream>
#include <iomanip>
#include <bitset>

player_ann::player_ann()
{
    // fann_print_parameters(ann);
    // fann_print_connections(ann);

    eng.seed(rd());
}

player_ann::~player_ann()
{
    fann_destroy(ann);
}

std::vector<int> player_ann::get_position()
{
    std::vector<int> out;
    for (size_t i = 0; i < 4; i++)
    {
        out.push_back(position[i]);
    }
    
    return out;
}

std::vector<float> player_ann::get_ann_weights()
{
    unsigned int num = fann_get_total_connections(ann);
    fann_type * weights;
    weights = (fann_type *)calloc(num, sizeof(*weights));
    std::vector<float> out;

    fann_get_weights(ann, weights);
    // std::cout << weights << std::endl;

    for (size_t i = 0; i < num; i++)
    {
        // std::cout << weights[i] << std::endl;
        out.push_back(weights[i]);
    }
    
    free(weights);
    return out;
}

void player_ann::set_ann_weights(std::vector<float> & weights)
{
    // std::cout << "Setting weights" << std::endl; 
    fann_set_weights(ann, &weights[0]);
}

int player_ann::get_num_hidden()
{
    return fann_get_total_neurons(ann) - fann_get_num_input(ann) - fann_get_num_output(ann);
}

int player_ann::make_decision()
{
    int choice;
    int64_t state = get_state();

    std::vector<float> state_vector;
    state_vector.resize(NUM_OF_STATES * 4);

    for (size_t i = 0; i < state_vector.size(); i++)
    {
        state_vector.at(i) = (state >> i) & 0x1;
        // state_vector.at(i) = state_vector.at(i) >> i;

        if (state_vector.at(i) == 0)
            state_vector.at(i) = -1;
    }
    
    // for (size_t i = 0; i < state_vector.size(); i++)
    // {
    //     std::cout << std::hex << state_vector.at(i);

    //     if (i == NUM_OF_STATES - 1 || i == 2 * NUM_OF_STATES - 1 || i == 3 * NUM_OF_STATES - 1)
    //         std::cout << ";\n";
    //     else
    //         std::cout << ", ";
    // }
    
    // std::cout << std::endl;

    fann_type * output = fann_run(ann, &state_vector[0]);

    // for (size_t i = 0; i < 4; i++)
    // {
    //     std::cout << output[i] << ", ";
    // }

    // std::cout << std::endl;

    float max_val = -10;

    for (size_t i = 0; i < 4; i++)
    {
        if (!is_valid_move(i))
            continue;

        // if (output[i] > max_val && position.at(i) != -1 && position.at(i) != 99)
        if (output[i] > max_val)
        {
            max_val = output[i];
            choice = i;
        }
    }

    int tmp;
    while (choice == -1)
    {
        std::uniform_int_distribution<> distr(0, 3); // define the range
        
        tmp = distr(eng);

        if (is_valid_move(tmp)) 
            choice = tmp;
    }
    // std::cout << "Indexes: " << position.at(0) << ", " << position.at(1) << ", "
    //           << position.at(3) <<  ", " << position.at(3) << "; chosen: " << token << std::endl;

    // std::cout << "Selected token: " << token << " Val: " << max_val << std::endl;
    return choice;
}

int64_t player_ann::get_state()
{
    // State start at nothing. Then go through if statements to see which states are relevant.
    // Do it for all four tokens
    int64_t state = 0;

    for (int i = 0; i < 4; i++)
    {
        int index = position[i];
        int new_pos = index + dice;

        if (index == -1 && dice != 6)
            new_pos = -1;

        if (index == -1 && dice == 6)
            new_pos = 0;

        int64_t token_state = 0;

        if (index == -1)
            token_state |= STATE_TOKEN_IS_DEAD;
        else
            token_state |= STATE_TOKEN_IS_FREE;        

        // Check if token is on star
        if ( index == 5  ||
             index == 11 ||
             index == 18 ||
             index == 24 ||
             index == 31 ||
             index == 37 ||
             index == 44 ||
             index == 50)
             token_state |= STATE_TOKEN_IS_ON_STAR;

        // Check if token is on globe
        if ((index - 8) % 13 == 0)
            token_state |= STATE_TOKEN_IS_ON_GLOBE;

        // Check if token is goal stretch
        if (index >= 51 && index != 99)
            token_state |= STATE_TOKEN_IS_ON_GOAL_STRETCH;
        
        // Check if token is in goal
        if (index == 99)
            token_state |= STATE_TOKEN_IS_FINISHED;
        
        // Check if token is on enemy start
        if ((index % 13 == 0) && index != 0)
            token_state |= STATE_TOKEN_IS_ON_ENEMY_START;

        // Check if token is forming blockage - two or more of same colour on same field.
        for (int j = 0; j < 4; j++)
        {
            if (i == j) continue;

            if (index == position[j] && index != -1)
                token_state |= STATE_TOKEN_IS_FORMING_BLOCKAGE;
        }

        // Check if token can form blockage - two or more of same colour on same field.
        for (int j = 0; j < 4; j++)
        {
            if (i == j) continue;

            if (new_pos == position[j] && new_pos != -1)
                token_state |= STATE_TOKEN_CAN_FORM_BLOCKAGE;
        }

        // Check if token can kill enemy or if there are more of them on the next spot
        int enemies = 0;

        // for (auto x : position)
            // std::cout << x << std::endl;

        for (int j = 4; j < 16; j++)
            if (new_pos == position[j] && new_pos != -1)
                enemies++;
                // token_state |= STATE_TOKEN_CAN_KILL_ENEMY;

        // std::cout << "Enemies: " << enemies << std::endl; 
        if (enemies == 1)
            token_state |= STATE_TOKEN_CAN_KILL_ENEMY;
        else if (enemies > 1)
            token_state |= STATE_TOKEN_CAN_DIE;        

        // Check if token can reach goal with this dice
        if (new_pos == 57)
            token_state |= STATE_TOKEN_CAN_REACH_GOAL;

        // Check if token can leave home
        if (dice == 6 && index == -1)
            token_state |= STATE_TOKEN_CAN_LEAVE_HOME;

        // Check if token can go to star
        if ( new_pos == 5  ||
             new_pos == 11 ||
             new_pos == 18 ||
             new_pos == 24 ||
             new_pos == 31 ||
             new_pos == 37 ||
             new_pos == 44 ||
             new_pos == 50)
             token_state |= STATE_TOKEN_CAN_GO_TO_STAR;

        // Check if token can go to globe
        if ((new_pos - 8) % 13 == 0)
        {
            for (int j = 4; j < 16; j++)
                if (new_pos == position[j] && new_pos != -1)
                { // If there is an enemy token on globe, then die.
                    token_state |= STATE_TOKEN_CAN_DIE;
                    break;
                }
            
            token_state |= STATE_TOKEN_CAN_GO_TO_GLOBE;
        }

        // Check if token can die
        // 1. If two enemies on the next position, done on line 130
        // 2. If enemy on globe, done on line 165
        

        // std::cout << (x << i * 12) << std::endl;
        // state |= token_state << i * 12;
        // std::cout << i * 12 << std::endl;
        token_state = token_state << i * NUM_OF_STATES;
        state |= token_state;
        
        // std::bitset<48> x1(token_state);     
        // std::bitset<48> x2(state);
        // std::cout << x1 << " " << x2 << " " << i * 12 << std::endl;
    }

    return state;
}