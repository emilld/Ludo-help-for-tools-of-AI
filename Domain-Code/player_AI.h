#ifndef PLAYER_AI_H
#define PLAYER_AI_H

#include "random"
#include "map"


#include "iplayer.h"
#include "game.h"


using namespace std;
class player_AI : public iplayer
{
private:
    
    std::mt19937 generator;
    std::uniform_int_distribution<int> distributionF;
    std::uniform_int_distribution<int> distributionI;

    float defaultMapReturn = 0;
    map<vector<int> ,float> Qvalues;
    int lastPickedPiece =-1;
    int pickedPiecePosition=-1;
    vector<int> lastState;
    float learningRate;
    int returnedAPiece=0;
    bool train;
    int lengthOfState=10;
    float eGreedy=1;
    int backwardsVision=6;
    float discount = 1;
    float rewardMovedPieceOutOfHome;
    float rewardReturnPiece;
    float rewardPieceCantGetOut;
    float rewardAtGlobe;
    string QName;


    bool isStar(int);
    bool isGlobe(int);
    bool canReturn(int);
    int enemiesBehind(int);
    void updateReward(vector<int> lastState,vector<int> currentState,float reward);
    vector<int> createState(int piece);
    float findQVal(vector<int> state);
    int make_decision();
    

/*
    int make_decision()
    {
        int test= 0;
        float ets = findQVal(createState(1)); 
        return -1;
    }
*/ 
public:
    player_AI()
    {
        std::random_device rd;
        generator = std::mt19937(rd());
    }
    player_AI(int length,bool toTrain,vector<float> dna,string QName);
    void saveMap();
    void loadMap();
    
    
    
};

#endif // PLAYER_AI_H
