#include "player_semismart.h"
#include <random>

int player_semismart::make_decision()
{
    if(dice == 6)
    {
        for(int i = 0; i < 4; ++i)
        {
            if(position[i]<0)
            {
                return i;
            }
        }
        for(int i = 0; i < 4; ++i)
        {
            if(position[i]>=0 && position[i] != 99)
            {
                return i;
            }
        }
    } 
    else 
    {
        for(int i = 0; i < 4; ++i)
        {
            if(position[i]>=0 && position[i] != 99)
            {
                return i;
            }
        }
        for(int i = 0; i < 4; ++i)
        { //maybe they are all locked in
            if(position[i]<0)
            {
                return i;
            }
        }
    }
    return -1;
}