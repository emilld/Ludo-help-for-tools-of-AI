#include <iostream>
#include "game.h"
#include "test_game.h"
#include "iplayer.h"
#include "player_random.h"
#include "player_semismart.h"
#include "player_ann.h"
#include "ga_trainer.h"
#include "player_ann_ga.h"
#include "player_AI.h"
#include <fstream>

// using namespace std;

#define TRAINING 0

int main()
{
    if (TRAINING)
    {
        ga_trainer ga(30, 250, 9);
        // Number of logical cores + 1
        // I have 8 in my pc
        ga.set_elitism(0.10);
        ga.set_mutation_rate(0.10);
        ga.set_tournament_games(250);
        std::string filename = "../output/description.txt";
        ga.write_description_file(filename);
        
        for (size_t i = 0; i < 10000; i++)
        {
            ga.generation_loop();
        }
    }
    else
    {  
        //Run the unit tests
        // test_game tester;
        // tester.run_all_tests();

        //Create players
        // player_ann_ga player_0(*new std::string("../output/09/generations/generation00999.csv"));
        // player_ann_ga player_1(*new std::string("../output/06/generations/generation00999.csv"));
        // player_ann_ga player_2(*new std::string("../output/02/generations/generation00999.csv"));
        // player_ann_ga player_0(*new std::string("../output/14/generations/generation04670.csv"));
        player_ann_ga player_0(*new std::string("../output/best/generations/generation06104.csv"));
        player_ann_ga player_2(*new std::string("../output/best/generations/generation06104.csv"));
        // player_ann player_0;
        // player_ann player_0;
        player_ann player_1, player_3;

        // against Q-players
        // learning rate, greedy, rewards[4]
        // // following from GA
        // vector<float> dna{ 0.913613 ,1 ,44.4719 ,-25.6989 ,-19.9736, 34.5904 };
        // player_AI player_2(10, false, dna, "");
        // player_AI player_3(10, false, dna, "");

        // Christian's manual values
        // float learningRate=0.005;
        // float greedy=1;
        // vector<float> dna{learningRate, greedy, 30.0, -100.0, -10.0, 30.0};

        // player_AI player_1(10, false, dna, "1");
        // player_AI player_3(10, false, dna, "1");

        // player_1.loadMap();
        // player_3.loadMap();

        // player_semismart player_2;
        // player_random player_3;

        //Play a game of Ludo
        game g(&player_0, &player_1, &player_2, &player_3);
        // g.play_game();
        // cout << "Player " << g.get_winner() << " won the game!" << endl << endl;

        // 
        std::ofstream file("../output/win_data.txt");
        // file << generation << ", " << population.at(0).fitness << ", " << mean << ", " << population.back().fitness << std::endl;
        file << "player 1, player 2, player 3, player 4" << std::endl;

        //Play many games of Ludo
        int wins[] = {0, 0, 0, 0};
        int games = 100000;

        for (int j = 0; j < 10; j++)
        {
            wins[0] = 0; wins[1] = 0; wins[2] = 0; wins[3] = 0;
            for(int i = 0; i < games; i++)
            {
                g.reset();
                g.set_first(i%4); //alternate who starts the game
                g.play_game();
                wins[g.get_winner()]++;

                if (i % (int)((float)games/100.) == 0)
                    std::cout << 100. * (float)i/(float)games << " [%]" << std::endl;
            }
            for(int i = 0; i < 4; i++)
            {
                std::cout << "Player " << i << " won " << wins[i] << " games, [" << 100 * wins[i] / (float)games << "%]." << std::endl; 
                
                if (i != 3)
                    file << wins[i] << ", ";
                else
                    file << wins[i];
            }
            std::cout << std::endl;
            file << std::endl;
        }

        file.close();
    }

    cout << "End of main" << endl;
    return 0;
}


