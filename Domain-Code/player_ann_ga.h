#ifndef PLAYER_ANN_GA_H
#define PLAYER_ANN_GA_H

#include <iostream>
#include <fstream>
#include <random>

#include "fann.h" // Fast Artificial Neural Network
#include "iplayer.h"
#include "player_ann.h"
#include "ga_trainer.h"

#include <fstream>

class player_ann_ga : public player_ann {
    private:
        void load_best_generation(std::string & filename)
        {
            std::ifstream file(filename.c_str());
            // std::ifstream file("../output/09/generations/generation00999.csv");

            // Read only first line
            Individual genom;

            std::string num;

            // std::getline(file, num);
            // std::cout << num << std::endl;

            for (size_t i = 0; i < fann_get_total_connections(ann); i++)
            {
                std::getline(file, num, ',');
                genom.weights.push_back(std::stod(num));
                // std::cout << num << ", ";
            }
            // std::cout << std::endl;
            
            // The last one is the fitness, hopefully
            std::getline(file, num, '\n');
            
            // std::cout << num << std::endl;
            genom.fitness = std::stod(num);

            // for (auto & i : genom.weights)
            // {
            //     std::cout << i << ", ";
            // }
            
            std::cout << std::endl;
            std::cout << "Fitness: " << genom.fitness << std::endl;

            file.close();
            set_ann_weights(genom.weights);
        }

    public:
        player_ann_ga(std::string & filename)
        {
            eng.seed(rd());
            load_best_generation(filename);

            std::cout << fann_get_total_neurons(ann) << std::endl;
        }
};

#endif // PLAYER_ANN_GA_H
